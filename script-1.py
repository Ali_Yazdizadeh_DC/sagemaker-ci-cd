import pandas as pd
from datetime import datetime, timezone
import json
import boto3
import botocore

BUCKET_NAME = 'sample-sagemaker-cicd' # replace with your bucket name
KEY = 'reports.csv' # replace with your object key

s3 = boto3.resource('s3')

try:
    s3.Bucket(BUCKET_NAME).download_file(KEY, 'reports-temp.csv')
    print('Successfully downloaded')
    # load reports df
    reports_df = pd.read_csv('reports-temp.csv')
    print('Reports loaded successfully')

except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == '404':
        print('The object does not exist.')
        print('Uploading reports template ...')
        s3.Bucket(BUCKET_NAME).upload_file('./reports.csv', KEY)
        print('Successfully uploaded')
        # load reports df
        reports_df = pd.read_csv('./reports.csv')
        print('Reports loaded successfully')

    else:
        raise

# add new report to reports.csv

### use UTC time to avoid timezone heterogeneity
date_time = datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S")
train_score = 0.9
test_score = 0.8
hyperparameters = {'lr':0.1,
                   'nestimators':100}

## add new row
new_report = pd.DataFrame({'date_time': [date_time], 'train_score': [train_score],
                           'test_score': [test_score], 'hyperparameters': [json.dumps(hyperparameters)],})

reports_df = reports_df.append(new_report)

## print new reports dataframe
print(reports_df)

## upload new reports dataframe
reports_df.to_csv('./reports-new.csv', index=False)
s3.Bucket(BUCKET_NAME).upload_file('./reports-new.csv', KEY)
