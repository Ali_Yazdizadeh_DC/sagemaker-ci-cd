import argparse, os, sys
import joblib
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split

# define logging
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

# define model_fn for deployment step
def model_fn(model_dir):
    model = joblib.load(os.path.join(model_dir, 'model.joblib'))
    return model

# define main training function
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--nestimators', type=int, default=100)
    parser.add_argument('--maxdepth', type=int, default=6)


    args, _ = parser.parse_known_args()
    nestimators = args.nestimators
    maxdepth = args.maxdepth

    model_dir = os.environ['SM_MODEL_DIR']
    training_dir = os.environ['SM_CHANNEL_TRAINING']

    # Load Data
    filename = os.path.join(training_dir, 'boston-housing.csv')
    logger.info(f'Args: {args}')
    logger.info(f'File Name: {filename}')
    data = pd.read_csv(filename)
    
    logger.info('Data loaded')

    X, y = data.iloc[:,1:], data.iloc[:,:1]
    
    # train test split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=123)
    logger.info('Train test split finished')
    
    # Fit the model
    model = RandomForestRegressor(n_estimators=nestimators,
                                  max_depth=maxdepth)
    model.fit(X_train, y_train)

    # evaluate model
    train_mse = mean_squared_error(model.predict(X_train), y_train)
    test_mse = mean_squared_error(model.predict(X_test), y_test)
    
    # log the metrics
    logger.info(f'Train_MSE={train_mse};')
    logger.info(f'Test_MSE={test_mse};')
    
    # Save the model
    model_path = os.path.join(model_dir, 'model.joblib')
    joblib.dump(model, model_path)


if __name__ == '__main__':
    main()
