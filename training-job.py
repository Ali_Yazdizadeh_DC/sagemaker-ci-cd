import sagemaker
from sagemaker.sklearn import SKLearn
from sagemaker.analytics import TrainingJobAnalytics
import boto3

session = sagemaker.Session(boto3.session.Session())

role_arn = 'arn:aws:iam::502452531878:role/YodaMaker' # replace with your IAM role arn
training_instance = 'ml.m5.large' # replace with your desired training istance

BUCKET_NAME = 'sample-sagemaker-cicd' # replace with your bucket name
PREFIX = 'boston-housing-regression'

source_folder = 's3://{}/{}/Source-Folders'.format(BUCKET_NAME, PREFIX)

training_data_s3_uri = 's3://{}/{}/boston-housing.csv'.format(BUCKET_NAME, PREFIX)
output_folder_s3_uri  = 's3://{}/{}/output/'.format(BUCKET_NAME, PREFIX)

metrics= [{'Name': 'test:mse', 'Regex': 'Test_MSE=(.*?);'},
          {'Name': 'train:mse', 'Regex': 'Train_MSE=(.*?);'},]

print('training data path:', training_data_s3_uri)
print('output data/model path:', output_folder_s3_uri)

boston_estimator = SKLearn(entry_point='training-script.py',
             role = role_arn,
             framework_version = '0.23-1',
             instance_count = 1, 
             instance_type = training_instance,
             output_path = output_folder_s3_uri,
             code_location = source_folder,
             metric_definitions=metrics,
             base_job_name = 'boston-housing-model',
             hyperparameters = {'nestimators':150,
                                'maxdepth':7},
             tags = [{"Key":"email",
                      "Value":"ali@datachef.co"}])

boston_estimator.fit({'training':training_data_s3_uri})

print(boston_estimator.latest_training_job.name)
print(boston_estimator.hyperparameters())
print({ key: boston_estimator.hyperparameters()[key] for key in boston_estimator.hyperparameters().keys() if 'sagemaker' not in key })
print(TrainingJobAnalytics(training_job_name=boston_estimator.latest_training_job.name, metric_names=['test:mse', 'train:mse']).dataframe())

## Update Reports file
import pandas as pd
from datetime import datetime, timezone
import json
import boto3
import botocore
import os

KEY = f'{PREFIX}/reports.csv'

s3 = boto3.resource('s3')

try:
    s3.Bucket(BUCKET_NAME).download_file(KEY, 'reports-temp.csv')
    print('Successfully downloaded')
    # load reports df
    reports_df = pd.read_csv('reports-temp.csv')
    print('Reports loaded successfully')

except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == '404':
        print('The object does not exist.')
        print('Uploading reports template ...')
        s3.Bucket(BUCKET_NAME).upload_file('./reports.csv', KEY)
        print('Successfully uploaded')
        # load reports df
        reports_df = pd.read_csv('./reports.csv')
        print('Reports loaded successfully')

    else:
        raise

# add new report to reports.csv
print('Starting to add new report to the reports.csv file located in S3')

### use UTC time to avoid timezone heterogeneity
date_time = datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S")

metrics_value = TrainingJobAnalytics(training_job_name=boston_estimator.latest_training_job.name, metric_names=['test:mse', 'train:mse']).dataframe()
matric_value_map = dict(zip(metrics_value['metric_name'], metrics_value['value']))

train_score = matric_value_map['train:mse']
test_score = matric_value_map['test:mse']

hyperparameters = { key: boston_estimator.hyperparameters()[key] for key in boston_estimator.hyperparameters().keys() if 'sagemaker' not in key }
training_job_name = boston_estimator.latest_training_job.name
region = os.getenv('AWS_DEFAULT_REGION')
endpoint_URL = f'https://runtime.sagemaker.{region}.amazonaws.com/endpoints/{training_job_name}/invocations'

## add new row
new_report = pd.DataFrame({'date_time': [date_time], 'train_score': [train_score],
                           'test_score': [test_score], 'hyperparameters': [json.dumps(hyperparameters)],
                           'training_job_name':[training_job_name], 'endpoint_URL':[endpoint_URL],})

reports_df = reports_df.append(new_report)

## print new reports dataframe
print(reports_df)

## upload new reports dataframe
reports_df.to_csv('./reports-new.csv', index=False)
s3.Bucket(BUCKET_NAME).upload_file('./reports-new.csv', KEY)
print(f'New reports.csv is accesible at: s3://{BUCKET_NAME}/{KEY}')
